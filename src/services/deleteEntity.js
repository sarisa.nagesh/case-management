const pool = require('../config/db');

async function removeEntity(EntityDetails) {
    const query = {
        name: 'delete Entity in db',
        text: `DELETE FROM "public".entities WHERE id=$1`,
        values: [
            EntityDetails.entity_id,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

module.exports = { removeEntity };