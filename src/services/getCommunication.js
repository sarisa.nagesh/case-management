const pool = require("../config/db");

async function getCommunication() {
  const query = {
    name: "Get all Communications",
    text: `SELECT * FROM "public".rev_communication order by message_id DESC`,
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { getCommunication };
