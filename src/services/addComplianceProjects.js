const pool = require("../config/db");

async function insertComplianceProjects(ComplianceProjectsDetails) {
  const query = {
    name: "insert Entity User in db",
    text: `INSERT INTO "public".Compliance_Projects (EID,Type,
            Name_EN,Name_AR,Consumer_Type,Desc_EN,Desc_AR,User_ID,Channel) 
                VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
    values: [
      ComplianceProjectsDetails.EID,
      ComplianceProjectsDetails.Type,
      ComplianceProjectsDetails.Name_EN,
      ComplianceProjectsDetails.Name_AR,
      ComplianceProjectsDetails.Consumer_Type,
      ComplianceProjectsDetails.Desc_EN,
      ComplianceProjectsDetails.Desc_AR,
      ComplianceProjectsDetails.User_ID,
      ComplianceProjectsDetails.Channel,
    ],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { insertComplianceProjects };
