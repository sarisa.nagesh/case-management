const pool = require("../config/db");

async function updateEntity(EntityDetails) {
  const query = {
    name: "update Entity in db",
    text: `UPDATE  "public".entities SET name=$2, short_name=$3,
        type_id=$4, focal_point_name=$5, focal_point_email=$6, focal_point_mobile=$7,
        notes=$8, extras=$9, active=$10, created_by=$11, updated_by=$12, created_at=$13, 
        updated_at=$14, ar_name=$15, description=$16, description_ar=$17, logo=$18 WHERE id=$1`,
    values: [
      EntityDetails.id,
      EntityDetails.name,
      EntityDetails.short_name,
      EntityDetails.type_id,
      EntityDetails.focal_point_name,
      EntityDetails.focal_point_email,
      EntityDetails.focal_point_mobile,
      EntityDetails.notes,
      EntityDetails.extras,
      EntityDetails.active,
      EntityDetails.created_by,
      EntityDetails.updated_by,
      EntityDetails.created_at,
      EntityDetails.updated_at,
      EntityDetails.ar_name,
      EntityDetails.description,
      EntityDetails.description_ar,
      EntityDetails.logo,
    ],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { updateEntity };
