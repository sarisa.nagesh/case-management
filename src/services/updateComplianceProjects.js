const pool = require('../config/db');

async function updateComplianceProjects(ComplianceProjectsDetails) {
    const query = {
        name: 'update ComplianceProjects in db',
        text: `UPDATE  "public".Compliance_Projects SET EID=$2, Type=$3,
        Name_EN=$4, Name_AR=$5, Consumer_Type=$6, Desc_EN=$7,
        Desc_AR=$8, User_ID=$9, Channel=$10 WHERE PID=$1`,
        values: [
            ComplianceProjectsDetails.PID,
            ComplianceProjectsDetails.EID,
            ComplianceProjectsDetails.Type,
            ComplianceProjectsDetails.Name_EN,
            ComplianceProjectsDetails.Name_AR,
            ComplianceProjectsDetails.Consumer_Type,
            ComplianceProjectsDetails.Desc_EN,
            ComplianceProjectsDetails.Desc_AR,
            ComplianceProjectsDetails.User_ID,
            ComplianceProjectsDetails.Channel,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}


module.exports = { updateComplianceProjects };
