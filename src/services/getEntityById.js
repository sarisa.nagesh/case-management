const pool = require("../config/db");

async function getEntityById(id) {
  const query = {
    name: "Get Entity ID",
    text: `SELECT * FROM "public".entities WHERE id = $1`,
    values: [id],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { getEntityById };
