const pool = require("../config/db");

async function insertEntity(EntityDetails) {
  const query = {
    name: "insert Entity in db",
    text: `INSERT INTO "public".Entity_Table (name,short_name,type_id,focal_point_name,
            focal_point_email,focal_point_mobile,notes,extras,active,created_by,updated_by,
            created_at,updated_at,ar_name,description,description_ar,logo) 
            VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, 17) RETURNING *`,
    values: [
      EntityDetails.name,
      EntityDetails.short_name,
      EntityDetails.type_id,
      EntityDetails.focal_point_name,
      EntityDetails.focal_point_email,
      EntityDetails.focal_point_mobile,
      EntityDetails.notes,
      EntityDetails.extras,
      EntityDetails.active,
      EntityDetails.created_by,
      EntityDetails.updated_by,
      EntityDetails.created_at,
      EntityDetails.updated_at,
      EntityDetails.ar_name,
      EntityDetails.description,
      EntityDetails.description_ar,
      EntityDetails.logo,
    ],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { insertEntity };
