const pool = require("../config/db");

async function removeEntityUser(EntityUserDetails) {
  const query = {
    name: "delete EntityUser in db",
    text: `DELETE FROM "public".Entity_Users WHERE User_ID=$1`,
    values: [EntityUserDetails.User_ID],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { removeEntityUser };
