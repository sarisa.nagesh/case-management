const pool = require('../config/db');

async function getallEntity() {
    const query = {
        name: 'Get all Entity',
        text: `SELECT * FROM "public".entities order by id DESC`,

    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}


module.exports = { getallEntity };