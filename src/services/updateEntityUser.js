const pool = require("../config/db");

async function updateEntityUser(EntityUserDetails) {
  const query = {
    name: "update EntityUser in db",
    text: `UPDATE  "public".Entity_Users SET Group_ID=$2, EID=$3,
        Full_Name=$4, Email=$5, Phone=$6, Role=$7,
        Profile_Pic=$8 WHERE User_ID=$1`,
    values: [
      EntityUserDetails.User_ID,
      EntityUserDetails.Group_ID,
      EntityUserDetails.EID,
      EntityUserDetails.Full_Name,
      EntityUserDetails.Email,
      EntityUserDetails.Phone,
      EntityUserDetails.Role,
      EntityUserDetails.Profile_Pic,
    ],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { updateEntityUser };
