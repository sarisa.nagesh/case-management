const pool = require("../config/db");

async function removeComplianceProjects(ComplianceProjectsDetails) {
  const query = {
    name: "delete ComplianceProjects in db",
    text: `DELETE FROM "public".Compliance_Projects WHERE PID=$1`,
    values: [ComplianceProjectsDetails.PID],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { removeComplianceProjects };
