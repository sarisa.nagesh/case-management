const pool = require("../config/db");

async function removeCommunication(CommunicationDetails) {
  const query = {
    name: "delete Communication in db",
    text: `DELETE FROM "public".rev_communication WHERE message_id=$1`,
    values: [CommunicationDetails.message_id],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { removeCommunication };
