const pool = require('../config/db');

async function insertEntityUser(EntityUserDetails) {
    const query = {
        name: 'insert Entity User in db',
        text: `INSERT INTO "public".users (first_name,last_name,email,
            encoded_password,username,role,active,authorization,created_by,updated_by,
            created_at,updated_at,entity_ids,temp_token,auth,phone,group_id) 
                VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12,
                     $13, $14, $15, $16, $17) RETURNING *`,
        values: [
            EntityUserDetails.first_name,
            EntityUserDetails.last_name,
            EntityUserDetails.email,
            EntityUserDetails.encoded_password,
            EntityUserDetails.username,
            EntityUserDetails.role,
            EntityUserDetails.active,
            EntityUserDetails.authorization,
            EntityUserDetails.created_by,
            EntityUserDetails.updated_by,
            EntityUserDetails.created_at,
            EntityUserDetails.updated_at,
            EntityUserDetails.entity_ids,
            EntityUserDetails.temp_token,
            EntityUserDetails.auth,
            EntityUserDetails.phone,
            EntityUserDetails.group_id,

        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

module.exports = { insertEntityUser };