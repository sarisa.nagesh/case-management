const pool = require("../config/db");

async function updateCommunication(CommunicationDetails) {
  const query = {
    name: "update Communication in db",
    text: `UPDATE  "public".rev_communication SET users=$2, subject=$3,
         purpose=$4, details=$5, group_id=$6 WHERE message_id=$1`,
    values: [
      CommunicationDetails.message_id,
      CommunicationDetails.users,
      CommunicationDetails.subject,
      CommunicationDetails.purpose,
      CommunicationDetails.details,
      CommunicationDetails.group_id,
    ],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { updateCommunication };
