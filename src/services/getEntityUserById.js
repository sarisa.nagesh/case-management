const pool = require("../config/db");

async function getEntityUserById(id) {
  const query = {
    name: "Get Entity User ID",
    text: `SELECT * FROM "public".users WHERE id = $1`,
    values: [id],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { getEntityUserById };
