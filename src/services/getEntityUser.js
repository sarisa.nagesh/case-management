const pool = require("../config/db");

async function getEntityUser() {
  const query = {
    name: "Get all Entity User",
    text: `SELECT * FROM "public".users order by id DESC`,
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { getEntityUser };
