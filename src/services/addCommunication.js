const pool = require("../config/db");

async function insertCommunication(CommunicationDetails) {
  const query = {
    name: "insert Communication in db",
    text: `INSERT INTO "public".rev_communication (users,subject,purpose,
            details,group_id) 
                VALUES($1, $2, $3, $4, $5) RETURNING *`,
    values: [
      CommunicationDetails.users,
      CommunicationDetails.subject,
      CommunicationDetails.purpose,
      CommunicationDetails.details,
      CommunicationDetails.group_id,
    ],
  };

  try {
    return await pool.query(query);
  } catch (error) {
    console.log(error);

    return error;
  }
}

module.exports = { insertCommunication };
