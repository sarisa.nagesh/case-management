const pool = require('../config/db');

async function getComplianceProjects() {
    const query = {
        name: 'Get all Compliance_Projects',
        text: `SELECT * FROM "public".Compliance_Projects order by PID DESC`,

    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}


module.exports = { getComplianceProjects };