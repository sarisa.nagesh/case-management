const winston = require('winston');
const jwt = require('jsonwebtoken');

const { removeCommunication } = require('../services/deleteCommunication');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/Communication.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const deleteCommunicationDetails = async (req, res) => {
    const CommunicationDetails = {
        message_id: req.body.message_id,

    };
    const deleteCommunication = await removeCommunication(CommunicationDetails);
    if (deleteCommunication.command === 'DELETE') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: deleteCommunication.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: deleteCommunication.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { deleteCommunicationDetails };