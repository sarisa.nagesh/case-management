const winston = require('winston');
const jwt = require('jsonwebtoken');
const parseIp = require('../middleware/parseIp');
const { insertComplianceProjects } = require('../services/addComplianceProjects');
const { getEntityById } = require('../services/getEntityById');
const { getEntityUserById } = require('../services/getEntityUserById');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/ComplianceProjects.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

//ComplianceProjectsRegister---------
const ComplianceProjectsRegister = async (req, res) => {
    logger.info(`received register request from ComplianceProjects with details ${req.body.PID}`);

    const { EID } = req.body;

    const EntityByIdDetails = await getEntityById(EID);

    if (EntityByIdDetails.rowCount === 0) {
        return res.json({
            statusCode: 404,
            message: 'Entity with that ID is not found',
        });
    }

    const { User_ID } = req.body;

    const EntityUserByIdDetails = await getEntityUserById(User_ID);

    if (EntityUserByIdDetails.rowCount === 0) {
        return res.json({
            statusCode: 404,
            message: 'EntityUser with that ID is not found',
        });
    }

    else {
    const ComplianceProjectsDetails = {
        EID,
        Type: req.body.Type,
        Name_EN: req.body.Name_EN,
        Name_AR: req.body.Name_AR,
        Consumer_Type: req.body.Consumer_Type,
        Desc_EN: req.body.Desc_EN,
        Desc_AR: req.body.Desc_AR,
        User_ID,
        Channel: req.body.Channel,

    };
    const secret = '!@#DWe$%^gge&&**';
    const token = jwt.sign({ sub: req.body.PID}, secret, {
        expiresIn: 86400, // expires in 24 hours
    });

    const addComplianceProjects = await insertComplianceProjects(ComplianceProjectsDetails);
    if (addComplianceProjects.command === 'INSERT') {
        logger.info(`addComplianceProjects added successfully`);

        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: [addComplianceProjects.rows,
                {
                    token: token,
                    ipAddress: parseIp(req)
                },]
            });
            
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: addComplianceProjects.detail,
            ipAddress: parseIp(req)
        });
    }
};

module.exports = { ComplianceProjectsRegister };