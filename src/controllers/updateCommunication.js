const winston = require("winston");
const jwt = require("jsonwebtoken");

const { updateCommunication } = require("../services/updateCommunication");
const parseIp = require("../middleware/parseIp");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/Communication.log",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

const updateCommunicationDetails = async (req, res) => {
  const CommunicationDetails = {
    message_id: req.body.message_id,
    users: req.body.users,
    subject: req.body.subject,
    purpose: req.body.purpose,
    details: req.body.details,
    group_id: req.body.group_id,
  };

  const updatecommunication = await updateCommunication(CommunicationDetails);
  if (updatecommunication.command === "UPDATE") {
    return res.status(200).json({
      statusCode: 200,
      message: "Sucess!",
      details: updatecommunication.rows,
      ipAddress: parseIp(req),
    });
  }

  return res.status(400).json({
    statusCode: 400,
    message: updatecommunication.detail,
    ipAddress: parseIp(req),
  });
};

module.exports = { updateCommunicationDetails };
