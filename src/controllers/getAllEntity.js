const winston = require('winston');
const jwt = require('jsonwebtoken');

const { getallEntity } = require('../services/getAllEntity');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/entities.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

// getallEntitydetails----------------------------------
const getallEntitydetails = async (req, res) => {
    const getEntity = await getallEntity();
    if (getEntity.command === 'SELECT') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: getEntity.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: getEntity.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { getallEntitydetails };