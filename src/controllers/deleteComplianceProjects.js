const winston = require('winston');
const jwt = require('jsonwebtoken');

const { removeComplianceProjects } = require('../services/deleteComplianceProjects');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/ComplianceProjects.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const deleteComplianceProjectsDetails = async (req, res) => {
    const ComplianceProjectsDetails = {
        PID: req.body.PID,

    };
    const deleteComplianceProjects = await removeComplianceProjects(ComplianceProjectsDetails);
    if (deleteComplianceProjects.command === 'DELETE') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: deleteComplianceProjects.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: deleteComplianceProjects.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { deleteComplianceProjectsDetails };