const winston = require('winston');
const jwt = require('jsonwebtoken');

const { removeEntity } = require('../services/deleteEntity');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/entities.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const deleteEntityDetails = async (req, res) => {
    const EntityDetails = {
        id: req.body.id,

    };
    const deleteEntity = await removeEntity(EntityDetails);
    if (deleteEntity.command === 'DELETE') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: deleteEntity.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: deleteEntity.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { deleteEntityDetails };