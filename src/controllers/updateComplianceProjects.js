const winston = require("winston");
const jwt = require("jsonwebtoken");

const { updateComplianceProjects } = require("../services/updateComplianceProjects");
const { getEntityById } = require('../services/getEntityById');
const { getEntityUserById } = require('../services/getEntityUserById');
const parseIp = require("../middleware/parseIp");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/ComplianceProjects.log",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

const updateComplianceProjectsDetails = async (req, res) => {
    
    const { EID } = req.body;

    const EntityByIdDetails = await getEntityById(EID);

    if (EntityByIdDetails.rowCount === 0) {
        return res.json({
            statusCode: 404,
            message: 'Entity with that ID is not found',
        });
    }

    const { User_ID } = req.body;

    const EntityUserByIdDetails = await getEntityUserById(User_ID);

    if (EntityUserByIdDetails.rowCount === 0) {
        return res.json({
            statusCode: 404,
            message: 'EntityUser with that ID is not found',
        });
    }

    else {
    const ComplianceProjectsDetails = {
        PID: req.body.PID,
        EID,
        Type: req.body.Type,
        Name_EN: req.body.Name_EN,
        Name_AR: req.body.Name_AR,
        Consumer_Type: req.body.Consumer_Type,
        Desc_EN: req.body.Desc_EN,
        Desc_AR: req.body.Desc_AR,
        User_ID,
        Channel: req.body.Channel,

    };

    const updatecomplianceProjects = await updateComplianceProjects(ComplianceProjectsDetails);
    if (updatecomplianceProjects.command === "UPDATE") {
        return res.status(200).json({
        statusCode: 200,
        message: "Sucess!",
        details: updatecomplianceProjects.rows,
        ipAddress: parseIp(req),
        });
    }

    return res.status(400).json({
        statusCode: 400,
        message: updatecomplianceProjects.detail,
        ipAddress: parseIp(req),
    });
    }
};

module.exports = { updateComplianceProjectsDetails };
