const path = require("path");
const express = require("express");
var fs = require("fs");
const multer = require("multer");
const Router1 = express.Router();
const DIR = "./images";
const entityimg = require("../services/entityfile");
const parseIp = require("../middleware/parseIp");

if (!fs.existsSync(DIR)) {
  fs.mkdirSync(DIR);
}

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    let fieldname = file.fieldname.split("|");
    let file_path = DIR + "/" + fieldname[0];
    console.log(file_path);

    if (!fs.existsSync(file_path)) {
      fs.mkdirSync(file_path);
      cb(null, file_path);
    } else {
      cb(null, file_path);
    }
  },
  filename: (req, file, cb) => {
    console.log("file.fieldname====>", file.fieldname);
    cb(null, "entity" + "-" + Date.now() + file.originalname);
  },
});

let Upload = multer({ storage: storage });

Router1.post("/UploadFile", Upload.any(), UploadFile);

async function UploadFile(req, res, next) {
  var response = req.files;
//   console.log('response+++++++++++++++req++bosy++++++++++',req.body.id);

  if (!req.files[0].fieldname) {
    console.log("No file received");
    return res.send({
      success: false,
    });
  } else {
    console.log("response", response[0]);
    try {
      const { path, mimetype } = response[0].path;
      entityimg.updateImagePath(response[0].path, req.body.id);
      res.send({
        message: "file uploaded successfully.",
        ipAddress: parseIp(req),
      });
    } catch (error) {
      res.status(400).send({
        message: "Error while uploading file. Try again later.",
        ipAddress: parseIp(req),
      });
    }
  }
}

module.exports = Router1;

