const winston = require('winston');
const jwt = require('jsonwebtoken');
const parseIp = require('../middleware/parseIp');
const { insertEntity } = require('../services/addEntity');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/entities.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

//EntityRegister---------
const EntityRegister = async (req, res) => {
    logger.info(`received register request from manager with details ${req.body.id}`);
    const EntityDetails = {
        name: req.body.name,
        short_name: req.body.short_name,
        type_id: req.body.type_id,
        focal_point_name: req.body.focal_point_name,
        focal_point_email: req.body.focal_point_email,
        focal_point_mobile: req.body.focal_point_mobile,
        notes: req.body.notes,
        extras: req.body.extras,
        active: req.body.active,
        created_by: req.body.created_by,
        updated_by: req.body.updated_by,
        created_at: req.body.created_at,
        updated_at: req.body.updated_at,
        ar_name: req.body.ar_name,
        description: req.body.description,
        description_ar: req.body.description_ar,
        logo: req.body.logo,
       
    };
    const secret = '!@#DWe$%^gge&&**';
    const token = jwt.sign({ sub: req.body.id}, secret, {
        expiresIn: 86400, // expires in 24 hours
    });

    const addEntity = await insertEntity(EntityDetails);
    if (addEntity.command === 'INSERT') {
        logger.info(`addEntity added successfully`);

        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: [addEntity.rows,
                {
                    token: token,
                    ipAddress: parseIp(req)
                },]
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: addEntity.detail,
            ipAddress: parseIp(req)
        });
};

module.exports = { EntityRegister };