const winston = require('winston');
const jwt = require('jsonwebtoken');

const { getEntityUser } = require('../services/getEntityUser');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/users.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

// getEntityUserdetails----------------------------------
const getEntityUserdetails = async (req, res) => {
    const getentityUsers = await getEntityUser();
    if (getentityUsers.command === 'SELECT') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: getentityUsers.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: getentityUsers.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { getEntityUserdetails };