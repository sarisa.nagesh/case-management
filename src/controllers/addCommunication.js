const winston = require("winston");
const jwt = require("jsonwebtoken");
const parseIp = require("../middleware/parseIp");
const { insertCommunication } = require("../services/addCommunication");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/Communication.log",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

//CommunicationRegister---------
const CommunicationRegister = async (req, res) => {
  logger.info(
    `received register request from Communication with details ${req.body.message_id}`
  );

  const CommunicationDetails = {
    users: req.body.users,
    subject: req.body.subject,
    purpose: req.body.purpose,
    details: req.body.details,
    group_id: req.body.group_id,
  };
  const secret = "!@#DWe$%^gge&&**";
  const token = jwt.sign({ sub: req.body.message_id }, secret, {
    expiresIn: 86400, // expires in 24 hours
  });

  const addCommunication = await insertCommunication(CommunicationDetails);
  if (addCommunication.command === "INSERT") {
    logger.info(`addCommunication added successfully`);

    return res.status(200).json({
      statusCode: 200,
      message: "Sucess!",
      details: [
        addCommunication.rows,
        {
          token: token,
          ipAddress: parseIp(req),
        },
      ],
    });
  }

  return res.status(400).json({
    statusCode: 400,
    message: addCommunication.detail,
    ipAddress: parseIp(req),
  });
};

module.exports = { CommunicationRegister };
