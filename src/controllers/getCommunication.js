const winston = require('winston');
const jwt = require('jsonwebtoken');

const { getCommunication } = require('../services/getCommunication');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/Communication.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

// getCommunicationdetails----------------------------------
const getCommunicationdetails = async (req, res) => {
    const getcommunications = await getCommunication();
    if (getcommunications.command === 'SELECT') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: getcommunications.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: getcommunications.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { getCommunicationdetails };