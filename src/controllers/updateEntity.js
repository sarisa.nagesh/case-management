const winston = require("winston");
const jwt = require("jsonwebtoken");

const { updateEntity } = require("../services/updateEntity");
const parseIp = require("../middleware/parseIp");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/entities.log",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

const updateEntityDetails = async (req, res) => {
  const EntityDetails = {
    id: req.body.id,
    name: req.body.name,
    short_name: req.body.short_name,
    type_id: req.body.type_id,
    focal_point_name: req.body.focal_point_name,
    focal_point_email: req.body.focal_point_email,
    focal_point_mobile: req.body.focal_point_mobile,
    notes: req.body.notes,
    extras: req.body.extras,
    active: req.body.active,
    created_by: req.body.created_by,
    updated_by: req.body.updated_by,
    created_at: req.body.created_at,
    updated_at: req.body.updated_at,
    ar_name: req.body.ar_name,
    description: req.body.description,
    description_ar: req.body.description_ar,
    logo: req.body.logo,
  };

  const updateentity = await updateEntity(EntityDetails);
  if (updateentity.command === "UPDATE") {
    return res.status(200).json({
      statusCode: 200,
      message: "Sucess!",
      details: updateentity.rows,
      ipAddress: parseIp(req),
    });
  }

  return res.status(400).json({
    statusCode: 400,
    message: updateentity.detail,
    ipAddress: parseIp(req),
  });
};

module.exports = { updateEntityDetails };
