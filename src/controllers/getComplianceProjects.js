const winston = require('winston');
const jwt = require('jsonwebtoken');

const { getComplianceProjects } = require('../services/getComplianceProjects');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/ComplianceProjects.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

// getComplianceProjectsdetails----------------------------------
const getComplianceProjectsdetails = async (req, res) => {
    const getcomplianceProjects = await getComplianceProjects();
    if (getcomplianceProjects.command === 'SELECT') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: getcomplianceProjects.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: getcomplianceProjects.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { getComplianceProjectsdetails };