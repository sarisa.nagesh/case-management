const winston = require("winston");
const jwt = require("jsonwebtoken");
const parseIp = require("../middleware/parseIp");
const { insertEntityUser } = require("../services/addEntityUser");
// const { getEntityById } = require("../services/getEntityById");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/EntityUser.log",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

//EntityUserRegister---------
const EntityUserRegister = async (req, res) => {
  logger.info(
    `received register request from EntityUser with details ${req.body.id}`
  );

  // const { EID } = req.body;

  // const EntityByIdDetails = await getEntityById(EID);

  // if (EntityByIdDetails.rowCount === 0) {
  //   return res.json({
  //     statusCode: 404,
  //     message: "Entity with that ID is not found",
  //   });
  // } else {
  const EntityUserDetails = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    encoded_password: req.body.encoded_password,
    username: req.body.username,
    role: req.body.role,
    active: req.body.active,
    authorization: req.body.authorization,
    created_by: req.body.created_by,
    updated_by: req.body.updated_by,
    created_at: req.body.created_at,
    updated_at: req.body.updated_at,
    entity_ids: req.body.entity_ids,
    temp_token: req.body.temp_token,
    auth: req.body.auth,
    auth: req.body.auth,
    group_id: req.body.group_id,
  };
  const secret = "!@#DWe$%^gge&&**";
  const token = jwt.sign({ sub: req.body.id }, secret, {
    expiresIn: 86400, // expires in 24 hours
  });

  const addEntityUser = await insertEntityUser(EntityUserDetails);
  if (addEntityUser.command === "INSERT") {
    logger.info(`addEntityUser added successfully`);

    return res.status(200).json({
      statusCode: 200,
      message: "Sucess!",
      details: [
        addEntityUser.rows,
        {
          token: token,
          ipAddress: parseIp(req),
        },
      ],
    });
  }

  return res.status(400).json({
    statusCode: 400,
    message: addEntityUser.detail,
    ipAddress: parseIp(req),
  });
  // }
};

module.exports = { EntityUserRegister };
