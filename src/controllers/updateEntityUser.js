const winston = require("winston");
const jwt = require("jsonwebtoken");

const { updateEntityUser } = require("../services/updateEntityUser");
// const { getallEntity } = require('../services/getAllEntity');
const parseIp = require("../middleware/parseIp");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/users.log",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

const updateEntityUserDetails = async (req, res) => {
    // const { EID } = req.body;

    // const EntityDetails = await getallEntity(EID);

    // if (EntityDetails.rowCount != 0) {
    //     return res.json({
    //         statusCode: 404,
    //         message: 'Entity with that ID is not found',
    //     });
    // }

    // else {
    const EntityUserDetails = {
            id: req.body.id,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            encoded_password: req.body.encoded_password,
            username: req.body.username,
            role: req.body.role,
            active: req.body.active,
            authorization: req.body.authorization,
            created_by: req.body.created_by,
            updated_by: req.body.updated_by,
            created_at: req.body.created_at,
            updated_at: req.body.updated_at,
            entity_ids: req.body.entity_ids,
            temp_token: req.body.temp_token,
            auth: req.body.auth,
            auth: req.body.auth,
            group_id: req.body.group_id,
    };

    const updateentityUser = await updateEntityUser(EntityUserDetails);
    if (updateentityUser.command === "UPDATE") {
        return res.status(200).json({
        statusCode: 200,
        message: "Sucess!",
        details: updateentityUser.rows,
        ipAddress: parseIp(req),
        });
    }

    return res.status(400).json({
        statusCode: 400,
        message: updateentityUser.detail,
        ipAddress: parseIp(req),
    });
    // }
};

module.exports = { updateEntityUserDetails };
