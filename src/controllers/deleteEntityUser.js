const winston = require('winston');
const jwt = require('jsonwebtoken');

const { removeEntityUser } = require('../services/deleteEntityUser');

const parseIp = require('../middleware/parseIp');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/users.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const deleteEntityUserDetails = async (req, res) => {
    const EntityUserDetails = {
        id: req.body.id,

    };
    const deleteEntityUser = await removeEntityUser(EntityUserDetails);
    if (deleteEntityUser.command === 'DELETE') {
        return res.status(200)
            .json({
                statusCode: 200,
                message: 'Sucess!',
                details: deleteEntityUser.rows,
                ipAddress: parseIp(req)
            });
    }

    return res.status(400)
        .json({
            statusCode: 400,
            message: deleteEntityUser.detail,
            ipAddress: parseIp(req)
        });

};

module.exports = { deleteEntityUserDetails };