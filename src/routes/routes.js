const express = require("express");
// const multer = require('multer');
const router = express.Router();
// const formDataHandler = multer();

//Entity Route
const Entity = require("../controllers/addEntity");
const getallEntitydetails = require("../controllers/getAllEntity");
const deleteEntityDetails = require("../controllers/deleteEntity");
const updateEntityDetails = require("../controllers/updateEntity");
const Router1 = require("../controllers/entityfile");


//EntityUser Route
const EntityUser = require("../controllers/addEntityUser");
const getEntityUserdetails = require("../controllers/getEntityUser");
const deleteEntityUserDetails = require("../controllers/deleteEntityUser");
const updateEntityUserDetails = require("../controllers/updateEntityUser");
const Router2 = require("../controllers/entityUserfile");

//Communication Route
const Communication = require("../controllers/addCommunication");
const getCommunicationdetails = require("../controllers/getCommunication");
const deleteCommunicationDetails = require("../controllers/deleteCommunication");
const updateCommunicationDetails = require("../controllers/updateCommunication");


//ComplianceProjects Route
const ComplianceProjects = require("../controllers/addComplianceProjects");
const getComplianceProjectsdetails = require("../controllers/getComplianceProjects");
const deleteComplianceProjectsDetails = require("../controllers/deleteComplianceProjects");
const updateComplianceProjectsDetails = require("../controllers/updateComplianceProjects");


//Started Api--------------------------------------------------

//Entity Api
router.post("/EntityRegister", Entity.EntityRegister);
router.get("/getallEntity", getallEntitydetails.getallEntitydetails);
router.post("/removeEntity", deleteEntityDetails.deleteEntityDetails);
router.post("/updateEntity", updateEntityDetails.updateEntityDetails);

router.post("/UploadFile", Router1);

//EntityUser Api
router.post("/EntityUserRegister", EntityUser.EntityUserRegister);
router.get("/getEntityUser", getEntityUserdetails.getEntityUserdetails);
router.post("/removeEntityUser", deleteEntityUserDetails.deleteEntityUserDetails);
router.post("/updateEntityUser", updateEntityUserDetails.updateEntityUserDetails);

router.post("/UploadFile", Router2);

//Communication Api
router.post("/CommunicationRegister", Communication.CommunicationRegister);
router.get("/getCommunication", getCommunicationdetails.getCommunicationdetails);
router.post("/removeCommunication", deleteCommunicationDetails.deleteCommunicationDetails);
router.post("/updateCommunication", updateCommunicationDetails.updateCommunicationDetails);

//ComplianceProjects Api
router.post("/ComplianceProjectsRegister", ComplianceProjects.ComplianceProjectsRegister);
router.get("/getComplianceProjects", getComplianceProjectsdetails.getComplianceProjectsdetails);
router.post("/removeComplianceProjects", deleteComplianceProjectsDetails.deleteComplianceProjectsDetails);
router.post("/updateComplianceProjects", updateComplianceProjectsDetails.updateComplianceProjectsDetails);


module.exports = router;

